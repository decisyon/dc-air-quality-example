(function()	{
	'use strict';

	function DCAirDataQualityController($scope, $timeout, $q, $http){

		var ctx = $scope.DECISYON.target.content.ctx,
			ctrl = this,
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
			FIX_INT_DCY = -1,
			descriptionsMap;

		/* Limiting the precision */
		var precisionCheck = function(value) {
			return parseFloat(value).toPrecision(4);
		};

		/* Managing of air quality Properties */
		var manageParamsFromContext = function(context) {
			var longitude = context.$longitude.value,
				radius = context.$radius.value,
				latitude = context.$latitude.value;
			ctrl.longitude	= (longitude !== EMPTY_PARAM_VALUE && longitude !== '' && longitude !== FIX_INT_DCY) ?  precisionCheck(longitude) : '';
			ctrl.radius	= (radius !== EMPTY_PARAM_VALUE && radius !== '' && longitude !== FIX_INT_DCY) ? radius : '';
			ctrl.latitude	= (latitude !== EMPTY_PARAM_VALUE && latitude !== '' && longitude !== FIX_INT_DCY) ? precisionCheck(latitude) : '';
		};

		/* Watch on widget context */
		var listenerOnDataChange = function() {
			$scope.$watch('DECISYON.target.content', function(newValue, oldValue) {
				if (!angular.equals(newValue, oldValue)) {
					manageParamsFromContext(newValue.ctx);
				}
			}, true);
		};

        var getParamsToParameters = function(latitude, longitude, radius){
            return {
				method  : 'GET',
				url     : 'https://api.openaq.org/v1/parameters'
			};
        };
        
        var getParamsToMeasurements = function(latitude, longitude, radius){
            return {
				method  : 'GET',
				url     : ('https://api.openaq.org/v1/measurements?coordinates=' + latitude + ',' + longitude + '&radius=' + radius)
			};
        };
        
        var getParametersDescriptionMap = function() {
		    var deferer = $q.defer();
		    
		    $timeout(function(){
		        if(!descriptionsMap){
        			$http(getParamsToParameters()).then(function successCallback(response) {
		                descriptionsMap = {};
        				angular.forEach(response.data.results, function(value, key) {
        					descriptionsMap[value.id] = value;
        				});
        				
        				deferer.resolve(descriptionsMap);
        			}, function errorCallback(rejection) {
        				deferer.reject(rejection);
        			});
    		    }
    		    else{
    		        deferer.resolve(descriptionsMap);
    		    }
		    });
		    
		    return deferer.promise;
		};
		
        var isValidMeasurementsParameters = function(latitude, longitude, radius){
            return (longitude !== '' && radius !== '' && latitude !== '');
        };
        
        var registerDataConnector = function(){
    		$scope.DECISYON.target.registerDataConnector(function(requestor) {
    		    
    			var getData = function() {
    			    
    			    var deferer = $q.defer();
    			    
    			    $timeout(function(){
        				/* Fetching the response only when latitude, longitude and radius parameters are passed */
        			    if (isValidMeasurementsParameters(ctrl.longitude, ctrl.radius, ctrl.latitude)) {
        			        
        			        getParametersDescriptionMap().then(function(descriptionsMap){
            					$http(getParamsToMeasurements(ctrl.latitude, ctrl.longitude, ctrl.radius)).then(function successCallback(response) {
            						var airData = [];
            						
            						angular.forEach(response.data.results, function(value, key) {
        								airData.push({
        								                'COUNTRY'   : value.country,
            											'CITY'      : value.city,
            											'LOCATION'  : value.location,
            											'PARAMETER' : descriptionsMap[value.parameter].description,
            											'UNIT'      : value.unit,
            											'VALUE'     : value.value,
            											'DATE'      : value.date.utc
        											});
        							});
            						deferer.resolve(airData);
            					}, function errorCallback(rejection) {
            						deferer.reject(rejection);
            					});
        			        }, function(rejection){
        			            deferer.reject(rejection);
        			        });
        					
        				} else {
        					/* Emptying the data*/
        					deferer.reject({
        					    error : {
        					        errorMsg : 'The parameters of measurements are not valid.'
        					    }
        					});
        				}
    			    });
    				
    				return deferer.promise;
    			};
    			
    			return {data : getData};
    		});
        };
        
        /* Initialize */
		var inizialize = function() {
			manageParamsFromContext(ctx);
			listenerOnDataChange();
			registerDataConnector();
		};
		

		inizialize();
	}

	DCAirDataQualityController.$inject = ['$scope','$timeout','$q' ,'$http'];
	DECISYON.ng.register.controller('dcAirDataQualityCtrl',DCAirDataQualityController);
}());