(function(ENV_VARS) {
	'use strict';
	var jsToken = ENV_VARS.getValue('jsToken');
	var cssToken= ENV_VARS.getValue('cssToken');
	var shLibToken= ENV_VARS.getValue('shLibToken');
	var APPLICATION_INFO = function(){
		this.jsToken 						= jsToken  !== '' ? ENV_VARS.getValue('jsToken').substring(1) + '/'  : '',
		this.cssToken 						= cssToken !== '' ? ENV_VARS.getValue('cssToken').substring(1) + '/' : '';
		this.shLibToken 					= shLibToken !== '' ? ENV_VARS.getValue('shLibToken').substring(1) + '/' : '';
		this.contextPath 					= ENV_VARS.variables.contextPath;
		this.isInDevelopment 				= ENV_VARS.variables.isInDevelopment;
		this.isInTesting					= ENV_VARS.variables.isInTesting;
		this.isInDevelopmentOrTesting		= ENV_VARS.variables.isInDevelopmentOrTesting,
		this.browserTimeout					= ENV_VARS.variables.browserTimeout;
		this.useMockData					= true;
		this.loadingScriptTimeout			= 5000;
		this.rootPath						= window.rootPath;
		this.domainUrl						= window.domainUrl;
		this.srcPath						= 'devel/';
		this.vendorPath						= 'vendor/';
		this.componentsPath					= 'modules/components/';
		this.sectionsPath					= 'modules/sections/';
		this.templatesPath					= 'templates/';
		this.bootstrapModulesPath			= 'bootstrap_modules/';
		this.assetsCssPath					= 'assets/css/';
		this.dcySdkApiPath					= this.bootstrapModulesPath + 'dcy_sdk_api/';
	};
	
	//Configuration Object
	var dcyAngular = window.dcyAngular = {
			'SESSION_DATA': {
				'data': {
					'applicationInfo': new APPLICATION_INFO()
				}
			},
			'internalDefine' : window.define //Store define of requirejs in internal variable
	};

	var applicationInfo 	= window.dcyAngular.SESSION_DATA.data.applicationInfo,
		vendorJsTokanized 	= applicationInfo.rootPath + applicationInfo.jsToken + applicationInfo.vendorPath,
		vendorCssTokanized 	= applicationInfo.rootPath + applicationInfo.cssToken + applicationInfo.vendorPath,
		srcJsTokanized 		= applicationInfo.rootPath + applicationInfo.jsToken + applicationInfo.srcPath,
		assetsPathTokanized = applicationInfo.rootPath + applicationInfo.cssToken + applicationInfo.srcPath + applicationInfo.assetsCssPath;
	
	require.config({
		map: {
			'*': {
				'css' : vendorCssTokanized + 'require-css/css.js'
			}
		},
		waitSeconds: applicationInfo.loadingScriptTimeout,
		paths: {
			// Require plugin
			lodash							: vendorJsTokanized + 'lodash/lodash',
			angular							: vendorJsTokanized + 'angular/angular',
			angularAnimate					: vendorJsTokanized + 'angular-animate/angular-animate.min',
			angularAria						: vendorJsTokanized + 'angular-aria/angular-aria.min',
			angularMaterial					: vendorJsTokanized + 'angular-material/angular-material',
			ngResource						: vendorJsTokanized + 'angular-resource/angular-resource.min',
			angularSanitize					: vendorJsTokanized + 'angular-sanitize/angular-sanitize',
			angularTranslate				: vendorJsTokanized + 'angular-translate/angular-translate',

			decisyonConfig					: srcJsTokanized + 'config/config',
			env								: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'env.provider',
			mockData						: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'mock_data.service',
			dp								: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'dp.service',
			dcyModule						: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'module.provider',

			renderingEnviroment				: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'rendering_enviroment.factory',
			dcyService						: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'dcy.factory', //Servizio(Factory) esposta all'utente

			dcyRequestErrorHandler			: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'request_error_handler.factory',
			dcyToastFactory					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'toast.factory',
			dcyHttpRequest					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'http_request.service',
			dcyHandledEvent					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'handled_event.service',
			dcyUploadRequestService			: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'upload_request.service',

			utils							: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'utils.factory',
			commonFn						: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'commonFn.factory',
			dcyFiltering					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'filtering.filters',
			recursionHelper					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'recursion_helper.factory',

			dcyTranslateFilter				: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'translate.filter',

			dcyNgRepeatEnd					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'ng_repeat_end.directive',

			templateManagerInterceptor		: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'template_manager.factory',
			templateManagerObject			: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'template_manager.object',
			templateManager					: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'template_manager.directive',
			rightClickManager				: srcJsTokanized + applicationInfo.bootstrapModulesPath + 'right_click.directive',

			mainCss							: assetsPathTokanized + 'main',
			appCtrl							: srcJsTokanized + 'app.controller',
			app								: srcJsTokanized + 'app'
		},
		shim: {
			'angular': {
				'exports': 'angular'
			},
			'decisyonConfig': ['angular'],
			'angularAnimate': ['angular', 'decisyonConfig'],
			'angularAria': ['angular', 'decisyonConfig'],
			'angularMaterial': ['angular', 'decisyonConfig'],

			'ngResource': ['angular'],

			//SANITIZE
			'angularSanitize': ['angular'],

			//TRANSLATE
			'angularTranslate': ['angular', 'decisyonConfig'],
			'dcyTranslateFilter': ['angular', 'angularTranslate'],

			//DCY
			'env': ['angular', 'decisyonConfig'],
			'mockData': ['angular', 'decisyonConfig'],
			'dp': ['angular', 'decisyonConfig'],
			'dcyModule': ['angular', 'decisyonConfig'],

			'renderingEnviroment': ['angular', 'decisyonConfig'],
			'dcyService': ['renderingEnviroment', 'angularTranslate'],

			'dcyRequestErrorHandler': ['angular', 'decisyonConfig', 'dcyToastFactory'],
			'dcyToastFactory': ['angular', 'decisyonConfig', 'angularMaterial'],
			'dcyHttpRequest': ['angular', 'decisyonConfig'],
			'dcyHandledEvent': ['angular', 'decisyonConfig'],
			'dcyUploadRequestService': ['angular', 'decisyonConfig'],

			'utils': ['angular', 'decisyonConfig'],
			'commonFn': ['angular', 'decisyonConfig'],

			'recursionHelper': ['angular', 'decisyonConfig'],

			'dcyNgRepeatEnd': ['angular', 'decisyonConfig'],
			'dcyFiltering': ['angular', 'decisyonConfig'],

			'templateManagerInterceptor': ['angular', 'decisyonConfig'],
			'templateManagerObject': {
				deps: ['angular', 'decisyonConfig'],
				exports: 'templateManagerObject'
			},
			'templateManager': {
				deps: ['angular', 'decisyonConfig', 'templateManagerObject', 'templateManagerInterceptor'],
				exports: 'templateManager'
			},
			'rightClickManager': ['angular', 'templateManager'],
			'app': ['angular', 'decisyonConfig', 'angularMaterial', 'ngResource', 'angularSanitize', 'angularTranslate'],
			'appCtrl': ['angular', 'decisyonConfig', 'app']
		},
		priority: ['lodash', 'angular', 'decisyonConfig']
	});
	require([
	         //angularjs libraries
	         'angular',

	         'lodash',

	         'css!mainCss',

	         // decisyon config
	         'decisyonConfig',  'angularAnimate', 'angularAria',
	         'angularMaterial', 'angularSanitize', 'ngResource', 'angularTranslate', 
	         //external others libraries
	         'env', 'mockData', 'dp', 'utils', 'commonFn', 

	         'renderingEnviroment', 'dcyService',

	         //Dcy Translation
	         'dcyTranslateFilter', 

	         'recursionHelper',
	         'dcyNgRepeatEnd',
	         'dcyFiltering',

	         'dcyRequestErrorHandler',
	         'dcyToastFactory',
	         'dcyHttpRequest',
	         'dcyHandledEvent',
	         'dcyUploadRequestService',


	         'templateManagerInterceptor', 'templateManagerObject', 'templateManager', 'rightClickManager', 'appCtrl', 'app', 'dcyModule'
	         ], function(angular) {
		angular.element(document).ready(function() {
			//Delete define AMD of requireJS for not to give a chance to use the deps imported by the user
			//window.define = undefined; 
			angular.bootstrap(document, ['dcyApp']);
		});
	});
}(window.ENV_VARS));
