(function(TemplateManager) {
	'use strict';
	
	function TemplateController($scope, dcyDPService, dcyEnvProvider, dcyUtilsFactory, dcyCommonFnFactory, $element) {
		
		
		var ctrl = this,
		
		serverResponse = {},

		templateManager = $scope.template = new TemplateManager($element[0], $scope),
		
		successHandler = function(response) {
			if (dcyCommonFnFactory.responseValidator(response)) {
				
				serverResponse = response;
				
				$scope.$emit('dcy.tmplMng.dataReceived');

				var details = {
					data 		: response.data,
					ctx 		: response.ctx,
					basePath 	: response.basePath,
					// path visibile dall'utente. Meglio che sia comprensivo del domain e del context root.
					resourcePath: dcyEnvProvider.getItem('domainUrl') + dcyEnvProvider.getItem('contextPath') + response.basePath,
					logicalName : response.view
				};
				
				templateManager.setDetails(details);
				
				//Espongo le APi
				templateManager.setApi();
				
				logToConsole('**************************template manager successHandler ********************');
				logToConsole(response);

				$scope.$parent.$emit('dcy.tmplMng.beforeRenderer', templateManager);
				
				var deps = ctrl.setDeps(response.deps);
				
				var cssDeps = [],
					jsBeforeDeps = [];
	
				if(deps){
					cssDeps = deps.css ? deps.css : cssDeps;
					jsBeforeDeps = deps.js && deps.js.before ? deps.js.before : jsBeforeDeps;
				}
				
				dcyUtilsFactory.resolveDependencies(cssDeps, templateManager.getBasePath()); 
				
				var dependenciesRequest = dcyUtilsFactory.resolveDependencies(jsBeforeDeps, templateManager.getBasePath(), function() {
					if(dcyUtilsFactory.isValidString($scope.onCompleteDepsLoaded)){
						eval($scope.onCompleteDepsLoaded);
					}
					templateManager.setTemplate(response.view, false);
				}, function(){
					templateManager.setTemplate(response.view, false);
				});

				if(!dependenciesRequest){
					templateManager.setTemplate(response.view, false);
				}
			}
			else{
				templateManager.setStatus(templateManager.statusEnum.ERROR);
				templateManager.setTemplate(templateManager.getErrorTemplateName());
			}
		},

		errorHandler = function(response) {
			var isInternal = response.view ? false : true;
						
			templateManager.setStatus(templateManager.statusEnum.ERROR);
			templateManager.setTemplate(!isInternal ? response.view : templateManager.getErrorTemplateName(), isInternal);
			$scope.error = response.response.data.error;			
			dcyUtilsFactory.errorToConsole('Response is not valid for elaboration : '+ response, 'error', 'dcyTemplate');
			$scope.$parent.$emit('decisyon.template_manager.error', templateManager);
		},

		bindListenerForTemplateRendering = function() {
			var errorTemplateLoaded = function(event, response){
				if(dcyUtilsFactory.manageHandlerQueue(event) && angular.equals(templateManager.getStatus(), templateManager.statusEnum.SUCCESS)){
					dcyUtilsFactory.errorToConsole('Response is not valid for elaboration : '+ response, 'dcyTemplate');
					templateManager.setStatus(templateManager.statusEnum.ERROR);
					templateManager.setTemplate(templateManager.getErrorTemplateName());
					//Call error callback
					/* jshint ignore:start */
					if(dcyUtilsFactory.isValidString($scope.onerror)){
						eval($scope.onerror);
					}
					/* jshint ignore:end */
					$scope.$parent.$emit('dcy.tmplMng.errorRenderer', templateManager);
				}
			},			
			includeContentErrorOff = $scope.$on('$includeContentError', errorTemplateLoaded),
			dcyErrorTemplateLoadedOff = $scope.$on('dcy.tmplMng.errorRenderer', errorTemplateLoaded),
			includeContentRequestOff = $scope.$on('$includeContentRequested', function(event){
				if(dcyUtilsFactory.manageHandlerQueue(event)){
					switch (templateManager.getTemplateName()) {
						case templateManager.getLoadingTemplateName():
							templateManager.setStatus(templateManager.statusEnum.STARTED);
							break;
						case templateManager.getErrorTemplateName():
							templateManager.setStatus(templateManager.statusEnum.ERROR);
						break;
						default:
							templateManager.setStatus(templateManager.statusEnum.SUCCESS);
							break;
					}
				}
			}),			
			completeTemplateLoadedOff = $scope.$on('$includeContentLoaded', function(event){
				if(dcyUtilsFactory.manageHandlerQueue(event)){
					var rendererStatus = templateManager.getStatus();
					if((angular.equals(rendererStatus, templateManager.statusEnum.SUCCESS) || angular.equals(rendererStatus, templateManager.statusEnum.ERROR))){
						dcyUtilsFactory.infoToConsole('Templare rendered success ', 'dcyTemplate');
						
						/* jshint ignore:start */
						if(dcyUtilsFactory.isValidString($scope.oncomplete)){
							eval($scope.oncomplete);
						}
						/* jshint ignore:end */
						$scope.$parent.$emit('dcy.tmplMng.loadedRenderer', templateManager);

						completeTemplateLoadedOff();
						dcyErrorTemplateLoadedOff();
						includeContentErrorOff();
						includeContentRequestOff();

						templateManager.setStatus(templateManager.statusEnum.COMPLETE);
					}
				}
			});
		},

		requestParams = {
				ctx: $scope.ctxIn,
				dpc: $scope.dpc,
				mockdata: $scope.mockdata
		},
		
		getCircularStyle = function(){			
			if(DSH_PAGE && dcyUtilsFactory.isValidString($element[0].id)){
				var parentElement 	= $element.closest('#'+$element[0].id + '.dshObjContainer')[0];
				
				if(parentElement){
					var height 			= parseInt(dcyUtilsFactory.getPropertyStyleByElement(parentElement, 'height'));
					var width 			= parseInt(dcyUtilsFactory.getPropertyStyleByElement(parentElement, 'width'));
					
					return Math.min(Math.min(height, width) * 0.5, 100);
				}
			}
			
			return 0;
		},
		
		runRequest = function(supplementCtx) {
			
			var ctx = requestParams.ctx && requestParams.ctx.ctx ? requestParams.ctx.ctx : null;
			
			//Merge supplement ctxIn in supplement ctx
			if(ctx){
				if(angular.isObject(supplementCtx)){
					angular.forEach(ctx, function(value, key) {
						addDataToCtx(supplementCtx, value.id, value.value); 
					});
					requestParams.ctx.ctx = supplementCtx;
				}
			}
			
			bindListenerForTemplateRendering();
			/* jshint ignore:start */
			if(dcyUtilsFactory.isValidString($scope.onstart)){
				eval($scope.onstart);
			}
			/* jshint ignore:end */
			if (dcyUtilsFactory.isValidObject($scope.staticData)) {
				if (angular.isDefined($scope.staticData.data) && angular.isDefined($scope.staticData.data.error)){
					//rootPage.attachPinesNotification(rootPage.PINES_NOTIFICATION_TYPE.ERROR, $scope.staticData.data.error.errorMsg);
					templateManager.setStatus(templateManager.statusEnum.ERROR);
					templateManager.setTemplate(templateManager.getErrorTemplateName());
					$scope.error = $scope.staticData.data.error;
				}else{
					if(angular.isDefined($scope.staticData.ctx)){
						$scope.staticData.ctx = dcyUtilsFactory.adapterForNewCtx($scope.staticData.ctx);
						successHandler($scope.staticData);
					}
				}
			} else {
				dcyDPService(requestParams).then(successHandler, errorHandler);
			}
		},
		
		bindStaticListenerForTemplateUpdate = function() {
			$scope.$on('dcy.tmplMng.updateUID', function(event, contentUID) {
				//Update data only if the content is changed in relation to uId block
				if(dcyUtilsFactory.manageHandlerQueue(event) && $scope.contentUID === contentUID) {
					update();
				}
			});
			
			$scope.$on('dcy.tmplMng.update', function(event, additionalCtx) {
				if(dcyUtilsFactory.manageHandlerQueue(event)){
					update(additionalCtx);
				}
			});
			
			$scope.$on('dcy.tmplMng.reinit', function(event) {
				if(dcyUtilsFactory.manageHandlerQueue(event)){
					init();
				}
			});
		},

		mergeAttributes = function(sourceElement, targetElement) {
			var arr = sourceElement[0].attributes;
			for(var i = 0; i < arr.length; i++) {
				var item = arr[i];
				if(!item.specified){
					continue;
				}
				
				var key = item.name;
				
				if(!angular.equals(key, 'id') && !angular.equals(key, 'objid')){
					continue;
				}
				
				var	sourceVal = item.value,
				targetVal = targetElement.attr(key);

				if(sourceVal === targetVal){
					continue;
				}

				var newVal = targetVal === undefined ? sourceVal : sourceVal + ' ' + targetVal;

				targetElement.attr(key, newVal);
			}
		},
		
		setTemplateLoading = function(){
			var ctx = requestParams.ctx && requestParams.ctx.ctx ? requestParams.ctx.ctx : null;
			
			var showLoader 	= dcyUtilsFactory.getCtxValue(ctx, 'showLoader');
			
			showLoader = dcyUtilsFactory.isValidObject(showLoader) ? angular.equals(showLoader.value, 'true') : angular.equals(showLoader, 'true');
			
			if(showLoader){
				var loaderDiameters = dcyUtilsFactory.getCtxValue(ctx, 'loaderDiameters');
				loaderDiameters = dcyUtilsFactory.isValidObject(loaderDiameters) ? loaderDiameters.value : loaderDiameters;
				$scope.circularDiameter = loaderDiameters ? loaderDiameters : getCircularStyle();
				templateManager.setTemplate(templateManager.getLoadingTemplateName());
			}
		},
		
		init = function(){
			setTemplateLoading();
			runRequest();
		},
		
		update = function(supplementCtx){
			runRequest(supplementCtx);
		},
		
		getSharedLibraries = function(sharedLibraries){
			var jsBeforeSharedLibs 	= [];
			var cssSharedLibs 		= [];
			
			angular.forEach(sharedLibraries, function(sharedLib, index){
				if(sharedLib.js){
					var jsBeforeLib = sharedLib.js.before;
					if(jsBeforeLib){
						jsBeforeSharedLibs = _.uniq(_.flatten([jsBeforeSharedLibs, jsBeforeLib]));
					}
				}
				if(sharedLib.css){
					var cssLib = sharedLib.css;
					if(cssLib){
						cssSharedLibs = _.uniq(_.flatten([cssSharedLibs, cssLib]));
					}
				}
			});
			
			return {
				js : {
					before 	: jsBeforeSharedLibs
				},
				css: cssSharedLibs
			}
		};
		
		ctrl.replaceWithChild = function(element) {
		    var child = angular.element(element[0].firstChild);
		    mergeAttributes(element, child);
		    element.replaceWith(child);
		};
		
		
		ctrl.getServerResponse = function(){
			return serverResponse;
		};
		
		ctrl.setDeps = function(deps){
			
			var Dependencies = function(deps){
				this.js = {
					before : new Array()
				};
				
				this.css = new Array();
				
				angular.merge(this, getSharedLibraries(deps.shared));
				
				this.css 		= _.uniq(_.flatten([this.css, deps.css ? deps.css : []]));
				
				if (deps.js){
					this.js.before 	= _.uniq(_.flatten([this.js.before, deps.js.before ? deps.js.before : []]));
				}
				
			};
			
			if(dcyUtilsFactory.isValidObject(deps)){
				return (ctrl.deps = new Dependencies(deps));
			}
			
			return undefined;
		};
		
		ctrl.getDeps = function(){
			return ctrl.deps;
		};
		
		bindStaticListenerForTemplateUpdate();
		
		init();
	}

	TemplateController.$inject = ['$scope', 'dcyDPService', 'dcyEnvProvider', 'dcyUtilsFactory', 'dcyCommonFnFactory', '$element'];
	
	function TemplatePreCompile(scope, iElement, iAttrs, controller, dcyUtilsFactory, $rootScope, dcyEnvProvider){
    	
		function appendInfoOnNode() {

			var serverResponse = controller.getServerResponse(),
				element = document.getElementById(iElement.get(0).id),
				nodeInfo;
			
			if (!element){
				return;
			}
			
			//Convert json object in commment
			function getCommentCode(json) {
				if (dcyUtilsFactory.isValidObject(json)) {
					var jsonString = angular.toJson(json, true);
					jsonString = dcyUtilsFactory.replaceAll(jsonString, '<', '&gt;');
					jsonString = dcyUtilsFactory.replaceAll(jsonString, '>', '&lt;');
					return document.createComment('DEBUG RENDERED DATA:' + jsonString);
				}

				return document.createComment('DEBUG RENDERED DATA: Data is not valid!' );
			}

			if (dcyEnvProvider.isInDevelopmentOrTesting() ) {
				var commentData = {
						contrUID 	: serverResponse.contrUID,
						contentUID	: serverResponse.contentUID,
						view		: serverResponse.view,
						engineV 	: serverResponse.engineV,
						data 		: serverResponse.data,
						ctx			: serverResponse.ctx						
				};
				//Rimuovo il commento gia presente, come per il caso refresh.
				//Ps.Il nodeType 8 specifica il nodo di tipo commento sul DOM.
				var firstChild = (element && element.firstChild) ? element.firstChild : null;
				if (firstChild && firstChild.nodeType === 8 && !!firstChild.nodeValue.match(/DEBUG RENDERED DATA/g)) {
					element.removeChild(firstChild);
				}
				
				element.insertBefore(getCommentCode(commentData), element.childNodes[0]);
			}

			nodeInfo = {
					selector : '#'+iElement.get(0).id,
					mainInfo: serverResponse.data,
					winRef : window,
					ctxIn : scope.ctxIn,
					engineV : serverResponse.engineV,
					templateInstance : scope.template
			};
			
			//TODO FRA/CHRI/MON
			nodeInfo.mainInfo.contentUID = serverResponse.contentUID;
			
			if(angular.isDefined(nodeInfo)){
				angular.element(element).data('nodeInfo', nodeInfo);
				rootPage.CONTENT_REFERENCE.addItem(serverResponse.contentUID, nodeInfo);
			}
			
		}

		var dataReceivedOff = scope.$on('dcy.tmplMng.dataReceived', function(){
			appendInfoOnNode();
			dataReceivedOff();
		});
		
	}
	
	/**
	 * @language ITA
     * @ngdoc directive
     * @name dcyApp.directive:dcyTemplate
     * @scope
     * @restrict E
     *
     * @description_IT
     * Ciao sono la desc in ita
     * 
     * @description_EN
     * Hi are the english doc
     *
     * @param_EN {string}  dpc   A dpc Param
     * @param_IT {string}  dpc   Il parametro dpc
     * @param_EN {object}  ctx   A ctx Param
     * @param_IT {object}  ctx   Il parametro ctx
     *
     */
	function DcyTemplateDirective(dcyUtilsFactory, $rootScope, dcyEnvProvider) {
		return {
			restrict: 'E',
			scope: {
				dpc						: '=dpc',
				ctxIn					: '=ctx',
				mockdata				: '@mockdata',
				staticData				: '=staticData',
				oncomplete 				: '=oncomplete',
				onCompleteDepsLoaded 	: '=onCompleteDepsLoaded',
				onerror 				: '=onerror',
				onstart 				: '=onstart',
				http					: '=http'
			},
			controller: TemplateController,
			controllerAs : 'tmplMngCtrl',
			template : '<dcycontent><dcytemplate ng-include src="template.getTemplatePath()"></dcytemplate></dcycontent>',
			compile: function compile() {
				return {
					pre: function preLink(scope, iElement, iAttrs, controller) { 
						new TemplatePreCompile(scope, iElement, iAttrs, controller, dcyUtilsFactory, $rootScope, dcyEnvProvider);
					},
					post : function(scope, tElement, tAttrs, controller){
						controller.replaceWithChild(tElement);
					}					
				};
			}
		};
	}

	DcyTemplateDirective.$inject = ['dcyUtilsFactory', '$rootScope', 'dcyEnvProvider'];
	angular.module('dcyApp.directives').directive('dcyTemplate', DcyTemplateDirective);
}(window.TemplateManager));
